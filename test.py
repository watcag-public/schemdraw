import schemdraw
import schemdraw.elements as elm
from schemdraw import logic
from schemdraw.parsing import logicparse


#d = schemdraw.Drawing()
#d.add(logic.Nand(inputs=3).label("NAND1"))
d = logicparse('not ((TRUE and (a xor x)) or ((y and c) and (z or FALSE)))', outlabel='Q')
d.save('drawing.svg')
